<?php
namespace App\Models;
use Nette;
final class dbManager{
    private $database;
    public function __construct(Nette\Database\Explorer $database)
	{   
		$this->database = $database;
		
	}

    public function vypisProdukt(){
        $data = $this->database->table('produkt')->fetchAll();
        return $data;
    }

    public function getUser($username){
        $row = $this->database->table('administrace')
			->where('jmeno', $username)
			->fetch();
        return $row;
    }
}
?>