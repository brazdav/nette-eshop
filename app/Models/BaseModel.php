<?php
namespace App\Models;
use Nette;
use Nette\Database\Table\Selection;

abstract class BaseModel
{
    private $dbexplorer;
    private $tableName;

    public function __construct(Nette\Database\Explorer $dbexplorer)
    {
        $this->dbexplorer = $dbexplorer;
        //jmeno tabulky s kterou budeme pracovat
        $this->tableName = $this->getTable();
    }

    //abstraktní metoda pro práci s názvem tabulky, díky ní můžeme nastavit jméno tabulky
    abstract function getTable();

    public function Insert($data) : void
    {
        $this->dbexplorer->table($this->tableName)->insert($data);
    }

    /**
     * funkce pro vkládání dat do tabulky
     * @param $id id záznamu pro změnu dat
     */
    public function Update(int $id, $data) : void
    {
        $this->dbexplorer->table($this->tableName)->where("id",$id)->update($data);
    }

    /**
     * funkce pro mazání dat z tabulky
     * @param $id id záznamu pro smazání
     */
    public function Delete($id) : void
    {
        $this->dbexplorer->table($this->tableName)->where("id",$id)->delete();
    }
}
