<?php
namespace App\Presenters;
use Nette;
use Nette\Security\SimpleIdentity;
use App\Models;

class MyAuthenticator implements Nette\Security\Authenticator
{
	private $passwords;
	private $dbManager;

	public function __construct(
		Models\DbManager $dbManager,
		Nette\Security\Passwords $passwords) {
		$this->dbManager = $dbManager;
		$this->passwords = $passwords;
	}

	public function authenticate(string $username, string $password): SimpleIdentity
	{
		$row = $this->dbManager->getUser($username);

		if (!$row) {
			throw new Nette\Security\AuthenticationException('User not found.');
		}

		if (!$this->passwords->verify($password, $row->password)) {
			throw new Nette\Security\AuthenticationException('Invalid password.');
		}

		return new SimpleIdentity(
			$row->id,
			$row->role, // nebo pole více rolí
			['name' => $row->username]
		);
	}
}