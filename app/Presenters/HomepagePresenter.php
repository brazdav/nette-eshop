<?php

declare(strict_types=1);

namespace App\Presenters;
use App\Models;
use Nette;


final class HomepagePresenter extends Nette\Application\UI\Presenter
{
    private $dbManager;

    public function __construct(Models\DbManager $dbManager)
    {
        $this->dbManager = $dbManager;
    }

    public function renderDefault(){
        $data = $this->dbManager->vypisProdukt();
        $this->template->produkt = $data;
    }
    
}
